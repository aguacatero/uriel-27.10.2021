FROM node:lts-buster-slim as develop

ARG COMMIT_HASH
WORKDIR /opt/app/

ADD package.json package-lock.json ./
RUN npm install

ADD public ./public
ADD src ./src

# Update the build's title to contain the build hash
RUN sed -i "s/\(Emoji\ Search\)/\1-${COMMIT_HASH}/g" ./src/Header.js
CMD [ "npm", "start"]


FROM develop as builder
RUN npm run build


FROM nginx:1.21
COPY --from=builder /opt/app/build/ /usr/share/nginx/html