.PHONY: help image install develop

PROJECT_VERSION := $(shell npm pkg get version | sed 's/"//g')

define HELPTEXT
Run "make <target>" where <target> is one of:
help:       
\t print this message
install:    
\t install the project for local development
image:      
\t build the project's production image
develop:    
\t run the project locally in a container
registry-login: 
\t log into the project's image registry
image-publish:  
\t build and push the project's prod image to the registry
endef
export HELPTEXT

help:
	@echo "$$HELPTEXT"

install:
	npm install

develop:
	docker build . \
		--target develop \
		--build-arg COMMIT_HASH=$(shell git rev-parse HEAD) \
		--tag emoji-search:develop &
	docker run -it -p 3000:3000 emoji-search:develop

image:
	# Builds an optimized production image 
	docker build . \
		--force-rm \
		--build-arg COMMIT_HASH=$(shell git rev-parse HEAD) \
		--tag emoji-search:$(PROJECT_VERSION)

registry-login:
	@docker login registry.gitlab.com \
		-u $(GITLAB_REGISTRY_USERNAME) -p $(GITLAB_REGISTRY_ACCESS_TOKEN)

image-publish: image
	docker tag emoji-search:$(PROJECT_VERSION) \
		registry.gitlab.com/aguacatero/emoji-search:$(PROJECT_VERSION) & 
	docker push registry.gitlab.com/aguacatero/emoji-search:$(PROJECT_VERSION)

k8s: image
	kubectl apply -f k8s/