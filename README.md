Emoji Search
---

Forked from https://github.com/ahfarmer/emoji-search

Prerequisites
---

To install locally, make sure that you have `npm`, `make`, and `Docker`
installed and running locally.

Quickstart
---

The project uses a simple `Makefile` to drive common development tasks. The
`Makefile` contains two useful targets:

`make develop` - Use this command to run a local, containerized instance of the
project available locally on port 3000. This target will use the project's
current code to build and run an image tagged as 'develop'.

`make image` - Use this command to build and tag the project's production     
image. The build is optimized to avoid installing dependencies unless they've 
changed so most builds are cheap.

Container Registry
---

To store a built image, you need to [create a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) granting access to `read_registry` and `write_registry`.
 
Once you have the token, export it and your username into the environment
variables `GITLAB_REGISTRY_ACCESS_TOKEN` and `GITLAB_REGISTRY_USERNAME`.
Take care not to expose the token to your shell's history. I recommend placing
these into a file and sourcing that file:

credentials.txt
```bat
export GITLAB_REGISTRY_ACCESS_TOKEN=YOUR_TOKEN
export GITLAB_REGISTRY_USERNAME=YOUR_NAME
```

```bat
source credentials.txt
```

Log into the registry using `make registry-login`.

Finally, use `make image-publish` to ensure the image is built and push the
image to GitLab. 

Kubernetes
---

To deploy the build into Kubernetes, use the `make k8s` build rule. This will
ensure the latest image is built, deploy it into a StatefulSet and create a 
Service exposing the service via http://localhost:31000.


Security
---

- I didn't think it was worth explicitly setting a user in the final image from
  the image build (the image building off of nginx) because nginx switches to
  using an internal nginx user. There is another image that explicitly supports
  unprivileged users (https://hub.docker.com/r/nginxinc/nginx-unprivileged). It
  seemed like a worse option due to its sparse documentation and unclear
  benefits.
  
- The project relies on many outdated packages. Those dependencies should be
  updated before this project is ever live in production.

- Pushing to the GitLab registry from dev laptops makes it trivial for faulty
  images to get published and for access tokens to get leaked (either on command
  line histories or within builds). Even though the note above about sourcing
  secrets will mitigate some of these issues (i.e. credentials.txt is gitignored
  and dockerignored) the ideal scenario would have docker builds and pushes
  running on a CI server whenever developer changes are merged.

- The static site should be listening on SSL.

- I spent no time looking at the qualities of the application or understanding
  any of its potential security holes.

Further Improvements
---

- I couldn't get my GitLab runner to execute the CI due to the error "The
  pipeline failed due to the user not being verified". I just added the pipeline
  file and ensured it passed linting.

- The GitLab CI should include docker in docker build steps and push steps
  whenever a merge request is accepted and closed.

- The K8s manifest files should support some form of templating (via kustomize)
  to allow flexible image deployments to the cluster. 